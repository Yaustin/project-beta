from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveBigIntegerField()
    vin = models.CharField(max_length=17, unique=True)


class Sale(models.Model):
    employee = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=1000)
    phone_number = models.PositiveBigIntegerField()


class SalesRecords(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name = "salesrecords",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name= "salesrecords",
        on_delete=models.CASCADE,
    )
    sales_price = models.PositiveBigIntegerField()

    # def __str__(self):
    #     return "$" + str(self.sales_price)
