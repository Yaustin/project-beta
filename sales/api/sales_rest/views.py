from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from sales_rest.encoders import PotentialCustomerEncoder, SaleDetailEncoder, SalesPersonListEncoder, AutomobileVODetailEncoder
from sales_rest.models import PotentialCustomer, Sale, SalesPerson, SalesRecords, AutomobileVO
import json


@require_http_methods(['GET', 'POST'])
def api_list_sales(request, automobile_vo_id=None):
    if request.method == 'GET':
        if automobile_vo_id is not None:
            sales = SalesRecords.objects.filter(automobile=automobile_vo_id)
        else:
            sales = SalesRecords.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            automobile_href=content['automobile']
            automobile=AutomobileVO.objects.get(import_href=automobile_href)
            content['automobile']=automobile

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=404
            )
        try:
            salesperson_id=content['salesperson']
            salesperson=SalesPerson.objects.get(id=salesperson_id)
            content['salesperson']=salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404
            )
        try:
            potential_customer_id=content['customer']
            potential_customer=PotentialCustomer.objects.get(id=potential_customer_id)
            content['customer']=potential_customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid potential customer id"},
                status=404
            )
        sales = SalesRecords.objects.create(**content)

        return JsonResponse(
            sales,
            encoder=SaleDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_sales(request, pk):
    if request.method == "GET":
        try:
            sales = SalesRecords.objects.get(id=pk)
            return JsonResponse(
                {"sales": sales},
                encoder=SaleDetailEncoder,
                safe=False
            )
        except SalesRecords.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404
            )
    elif request.method == "DELETE":
        count, _ = SalesRecords.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                automobile = SalesRecords.objects.get(id=content["automobile"])
                content["automobile"] = automobile
        except SalesRecords.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=404
            )
        SalesRecords.objects.filter(id=pk).update(**content)
        sales = Sale.objects.get(id=pk)
        return JsonResponse(
            sales,
            encoder=SaleDetailEncoder,
            safe=False
        )


@require_http_methods(['GET', 'POST'])
def api_list_salesperson(request):
    if request.method == 'GET':
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalesPersonListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonListEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            salesperson = SalesPerson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid, this salesperson does not exist"},
                status=404
            )
        SalesPerson.objects.filter(id=pk).update(**content)
        sales = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales,
            encoder=SalesPersonListEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_potential_customer(request):
    if request.method == 'GET':
        potential_customer = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customer": potential_customer},
            encoder=PotentialCustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = PotentialCustomer.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=PotentialCustomerEncoder,
            safe=False
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_detail_potential_customers(request, pk):
    if request.method == "GET":
        try:
            potential_customer = PotentialCustomer.objects.get(id=pk)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    elif request.method == "DELETE":
        count, _ = PotentialCustomer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            potential_customer = PotentialCustomer.objects.get(id=content["customer"])
            content["customer"] = potential_customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid, this potential customer does not exist"},
                status=404
            )
        PotentialCustomer.objects.filter(id=pk).update(**content)
        potential_customer = PotentialCustomer.objects.get(id=pk)
        return JsonResponse(
            potential_customer,
            encoder=SalesPersonListEncoder,
            safe=False
        )

@require_http_methods(["GET"])
def api_unsold_vehicles(request):
    vin_in_sales = []
    for sale in SalesRecords.objects.all():
        vin_in_sales.append(sale.automobile.vin)
    unsold_vehicles=AutomobileVO.objects.exclude(vin__in=vin_in_sales)
    return JsonResponse(
        {'unsold_vehicles': unsold_vehicles},
        encoder= AutomobileVODetailEncoder,
        safe=False
    )
