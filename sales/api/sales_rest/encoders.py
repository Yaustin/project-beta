from common.json import ModelEncoder
from sales_rest.models import PotentialCustomer, SalesPerson, SalesRecords, AutomobileVO


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name", "address", "phone_number", "id"]


class SalesPersonListEncoder(ModelEncoder):
    model=SalesPerson
    properties=["name", "employee_number", "id"]


class AutomobileVODetailEncoder(ModelEncoder):
    model=AutomobileVO
    properties = ["import_href", "vin"]


class SaleDetailEncoder(ModelEncoder):
    model = SalesRecords
    properties = ["salesperson", "customer", "automobile", "sales_price", "id"]
    encoders ={"customer": PotentialCustomerEncoder(),
        "salesperson": SalesPersonListEncoder(),
        "automobile": AutomobileVODetailEncoder()}
