from django.urls import path
from .views import api_detail_potential_customers, api_detail_sales, api_detail_salesperson, api_list_potential_customer, api_list_sales, api_list_salesperson, api_unsold_vehicles\



urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("automobiles/<int:automobile_vo_id>/sales/", api_list_sales, name="api_create_sales"),
    path("sales/<int:pk>/", api_detail_sales, name="api_detail_sales"),
    path("salesperson/", api_list_salesperson, name="api_list_salesperson"),
    path("salesperson/<int:pk>/", api_detail_salesperson, name="api_detail_salesperson"),
    path("salesperson/", api_list_salesperson, name="api_create_salesperson"),
    path("potential_customers/", api_list_potential_customer, name="api_list_potential_customers"),
    path("potential_customers/", api_list_potential_customer, name="api_create_potential_customers"),
    path("potential_customers/<int:pk>/", api_detail_potential_customers, name="api_detail_potential_customers"),
    path('automobiles/unsold/', api_unsold_vehicles, name="unsold_vehicles")
]
