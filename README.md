# CarCar

Team:

- Soslan - Service
- Yonah - Sales

## Design
1. Frontend - Javascript, Bootsrap and React
2. Backend - Python and Django
## Service microservice
In service there are three different models.
There is a Technician model with the properties of name and employee_number.

An Appointment model with the following properties: customer name, date, time, vin, reason, and technician as a foregin key relating to the Technician model.

There is also an AutomobileVO model with a vin property. This value object is utilized in the poller to pull data from the Automobile model in inventory microservice to the service microservice.

## Sales microservice
Created several models for sales.
Sale model consists of an employee, customer, and automobile property with automobile being the forgeing key.

The foreign key was created using a Value Object for the Automobile  model in inventory microservice. The AutomobileVO consists of an href and vin. This value object is utilized in several view functions and it allowed us to get the data from the inventory microservice to the sales microservice.

I have a Salesperson model with the following properties: name and employee number.

A PotentialCustomer model with the following properties: name, address, and phone number.

A SalesRecord model that has three forign keys automobile, salesperson, and customer each of which relates to the above modeels and a sales price property.

## Installation Instructions
1. Clone the repository
2. CD into a new project directory
3. Run docker volume create beta-data
4. Run docker compose build
5. Run docker compose up

## Using the application
The features can be viewed on localhost:3000/.
