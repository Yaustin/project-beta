import { useState, useEffect } from 'react'

const AppointmentsHistory = () => {
  const [filterValue, setFilterValue] = useState(' ')
  const [filterKey, setFilterKey] = useState('vin')

  const [appointments, setAppointments] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/')

    if (response.ok) {
      const data = await response.json()
      setAppointments(data.appointments)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const handleChange = (e) => {
    setFilterValue(e.target.value)
  }

  const filteredAppointments = () => {
    return appointments.filter((appointment) =>
      appointment[filterKey].toLowerCase().includes(filterValue)
    )
  }

  return (
    <>
      <div className="d-flex p-2">
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossOrigin="anonymous"
        ></link>

        <input
          className="form-control mr-sm-2"
          type="text"
          placeholder="Search VIN #"
          aria-label="Search"
          onChange={handleChange}
        />
      </div>

      <h1>Service Appointments </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>

        <tbody>
          {filteredAppointments().map((appointment) => {
            return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer_name}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.technician.name}</td>
                <td>{appointment.reason}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default AppointmentsHistory
