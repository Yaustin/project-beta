import { useEffect, useState } from 'react'

function AppointmentsList() {
  const [appointments, setAppointments] = useState([])
  const [vins, setVins] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/')

    if (response.ok) {
      const data = await response.json()
      setAppointments(data.appointments)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const vinData = async () => {
    const secondResponse = await fetch('http://localhost:8080/api/vins/')

    if (secondResponse.ok) {
      const loadedVins = await secondResponse.json()
      setVins(loadedVins.vins)
    }
  }

  useEffect(() => {
    vinData()
  }, [])

  async function cancelAppointment(id) {
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/`,
      {
        method: 'DELETE',
        body: JSON.stringify(appointments),
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    if (response.ok) {
      getData()
    }
  }

  async function completeAppointment(id) {
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/`,
      {
        method: 'PUT',
        body: JSON.stringify({ completed: 'True' }),
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    if (response.ok) {
      getData()
    }
  }

  return (
    <>
      <h1>Service Appointments </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIP status</th>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            if (appointment.completed === false)
              return (
                <tr key={appointment.id}>
                  {appointment.is_vip && <td>VIP</td>}
                  {!appointment.is_vip && <td></td>}
                  <td>{appointment.vin}</td>
                  <td>{appointment.customer_name}</td>
                  <td>{appointment.date}</td>
                  <td>{appointment.time}</td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    <button
                      className="btn btn btn-danger"
                      onClick={() => cancelAppointment(appointment.id)}
                    >
                      cancel
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn btn btn-success"
                      onClick={() => completeAppointment(appointment.id)}
                    >
                      finished
                    </button>
                  </td>
                </tr>
              )
          })}
        </tbody>
      </table>
    </>
  )
}

export default AppointmentsList
