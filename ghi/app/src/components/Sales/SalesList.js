import { useEffect, useState } from 'react';


function SalesList () {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/")

        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }

    useEffect(() =>{
        getData()
    })

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee number</th>
              <th>Customer name</th>
              <th>Vin</th>
              <th>Sales price</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sale, id) => {
              return (
                <tr key={id}>
                  <td>{sale.salesperson.name}</td>
                  <td>{ sale.salesperson.employee_number }</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.sales_price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>

    );

}

export default SalesList
