import { useEffect, useState } from 'react';


function SalespersonSaleList () {
    const [filterValue, setFilterValue] = useState('')
    const [sales, setSales] = useState([])


    const getData = async () => {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url)


        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)

        }
    }
    useEffect(() =>{
        getData()
    }, [])

    const handleChangeForm = (e) => {
        setFilterValue(e.target.value)
    }
    const filteredSalespeople = () => {
        if (filterValue === '') {
            return sales
        } else {
            return sales.filter((sale) =>
                sale.salesperson.name.toLowerCase().includes(filterValue)
            )
        }
    }

    return (
        <>
            <div className="row">
                <link
                    href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
                    crossOrigin="anonymous"
                ></link>
            <h1>Sales person history</h1>
            <input onChange={handleChangeForm} placeholder="Filter For Salesperson" />
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Salesperson</th>
                    <th>Employee number</th>
                    <th>Customer name</th>
                    <th>Vin</th>
                    <th>Sales price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSalespeople().map((sale, employee_number) => {
                        return (
                            <tr key={employee_number}>
                            <td>{sale.salesperson.name}</td>
                            <td>{ sale.salesperson.employee_number }</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.sales_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
        </>

    );

}

export default SalespersonSaleList
