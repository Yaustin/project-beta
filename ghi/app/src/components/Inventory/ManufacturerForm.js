import React, { useState, useEffect } from 'react'

function ManufacturerForm() {
  const [manufacturers, setManufacturers] = useState([])

  const [formData, setFormData] = useState({
    name: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault()

    const url = 'http://localhost:8100/api/manufacturers/'

    const fetchConfig = {
      method: 'post',
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig)

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({
        name: '',
      })
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value
    const inputName = e.target.name

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value,
    })
  }

  return (
    <div className="row">
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossOrigin="anonymous"
      ></link>
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ManufacturerForm
