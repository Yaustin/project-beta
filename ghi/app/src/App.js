import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import ManufacturersList from './components/Inventory/ManufacturersList'
import ManufacturerForm from './components/Inventory/ManufacturerForm'
import VehicleModelsList from './components/Inventory/ModelsList'
import ModelForm from './components/Inventory/ModelForm'
import AutosList from './components/Inventory/AutosList'
import AutoForm from './components/Inventory/AutoForm'
import TechnicianForm from './components/Appointments/TechnicianForm'
import AppointmentForm from './components/Appointments/AppointmentForm'
import AppointmentsList from './components/Appointments/AppointmentsList'
import AppointmentsHistory from './components/Appointments/AppointmentsHistory'
import SalespersonForm from './components/Sales/SalespersonForm'
import PotentialCustomerForm from './components/Sales/PotentialCustomerForm'
import SalesList from './components/Sales/SalesList'
import SalesForm from './components/Sales/SalesForm'
import SPList from './components/Sales/SPList'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
            <Route index element={<VehicleModelsList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="autos">
            <Route index element={<AutosList />} />
            <Route path="new" element={<AutoForm />} />
          </Route>

          <Route path="/technician" element={<TechnicianForm />} />

          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentsHistory />} />
          </Route>

          <Route path="salesperson">
            <Route path="new" element={<SalespersonForm />} />
            <Route index element={<SPList />} />
          </Route>

          <Route
            path="/potential_customer"
            element={<PotentialCustomerForm />}
          />

          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
