from django.db import models
from django.urls import reverse
# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)


class Appointment(models.Model):
    customer_name = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    vin = models.CharField(max_length=17)
    reason = models.CharField(max_length=100, blank=True
    )
    completed = models.BooleanField(default=False)
    is_vip = models.BooleanField(default=False)



    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.customer_name

    class Meta:
        ordering = ("date", "customer_name")
