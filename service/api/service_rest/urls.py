from django.urls import path
from .views import api_appointments, api_technicians, api_appointment, api_inventory_vin

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointments, name="api_appointments"),
     path(
        "appointments/<int:pk>/",
        api_appointment,
        name="api_appointment",
    ),
    path("vins/", api_inventory_vin, name="api_inventory_vins"),


]
