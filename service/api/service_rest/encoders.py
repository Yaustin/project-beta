
from common.json import ModelEncoder
from .models import Appointment, Technician, AutomobileVO


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "name"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer_name",
	    "date",
	    "time",
        "vin",
        "technician",
        "reason",
        "completed",
        "is_vip",

    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
